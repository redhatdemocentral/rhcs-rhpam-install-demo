#!/bin/sh 
DEMO="Red Hat Process Automation Manager Install Demo"
AUTHORS="Andrew Block, Eric D. Schabell, Duncan Doyle"
PROJECT="git@gitlab.com:redhatdemocentral/rhcs-rhpam-install-demo.git"
SRC_DIR=./installs
OC_URL="https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest-4.3"

# Adjust these variables to point to an OCP instance.
OPENSHIFT_USER=developer
OPENSHIFT_PWD=developer
HOST_IP=api.crc.testing   # set with CRC instance hostname or IP.
HOST_APPS=apps-crc.testing  
HOST_PORT=6443
OCP_APP=rhcs-rhpam-install-demo
OCP_PRJ=appdev-in-cloud

KIE_ADMIN_USER=erics
KIE_ADMIN_PWD=redhatpam1!
MEM_LIMIT=2Gi
VERSION=78

# prints the documentation for this script.
function print_docs() 
{
	echo "The default option is to run this using Code Ready Containers, an OpenShift Container"
	echo "Platform for your local machine. This host has been set by default in the variables at"
	echo "the top of this script. You can modify if needed for your own host and ports by mofifying"
	echo "these variables:"
	echo
	echo "    HOST_IP=api.crc.testing"
  echo "    HOST_PORT=6443"
	echo
	echo "It's also possible to install this project on a personal Code Ready Container installation, just point"
  echo "this installer at your installation by passing an IP address of the hosting cluster:"
	echo
	echo "   $ ./init.sh IP"
	echo
	echo "IP could look like: 192.168.99.100"
	echo
	echo "Both methodes are validated by the install scripts."
	echo
}

# check for a valid passed IP address.
function valid_ip()
{
	local  ip=$1
	local  stat=1

	if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
		OIFS=$IFS
		IFS='.'
		ip=($ip)
		IFS=$OIFS
		[[ ${ip[0]} -le 255 && ${ip[1]} -le 255 && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
		stat=$?
	fi

	return $stat
}

# wipe screen.
clear 

echo
echo "######################################################################"
echo "##                                                                  ##"   
echo "##  Setting up the ${DEMO}  ##"
echo "##                                                                  ##"   
echo "##             ####  ##### ####     #   #  ###  #####               ##"
echo "##             #   # #     #   #    #   # #   #   #                 ##"
echo "##             ####  ###   #   #    ##### #####   #                 ##"
echo "##             #  #  #     #   #    #   # #   #   #                 ##"
echo "##             #   # ##### ####     #   # #   #   #                 ##"
echo "##                                                                  ##"
echo "##           ####  ####   ###   #### #####  ####  ####              ##"
echo "##           #   # #   # #   # #     #     #     #                  ##"
echo "##           ####  ####  #   # #     ###    ###   ###               ##"
echo "##           #     #  #  #   # #     #         #     #              ##"
echo "##           #     #   #  ###   #### ##### ####  ####               ##"
echo "##                                                                  ##"
echo "##   ###  #   # #####  ###  #   #  ###  ##### #####  ###  #   #     ##"
echo "##  #   # #   #   #   #   # ## ## #   #   #     #   #   # ##  #     ##"
echo "##  ##### #   #   #   #   # # # # #####   #     #   #   # # # #     ##"
echo "##  #   # #   #   #   #   # #   # #   #   #     #   #   # #  ##     ##"
echo "##  #   # #####   #    ###  #   # #   #   #   #####  ###  #   #     ##"
echo "##                                                                  ##"
echo "##           #   #  ###  #   #  ###  ##### ##### ####               ##"
echo "##           ## ## #   # ##  # #   # #     #     #   #              ##"
echo "##           # # # ##### # # # ##### #  ## ###   ####               ##"
echo "##           #   # #   # #  ## #   # #   # #     #  #               ##"
echo "##           #   # #   # #   # #   # ##### ##### #   #              ##"
echo "##                                                                  ##" 
echo "##                 #### #      ###  #   # ####                      ##"
echo "##            #   #     #     #   # #   # #   #                     ##"
echo "##           ###  #     #     #   # #   # #   #                     ##"
echo "##            #   #     #     #   # #   # #   #                     ##"
echo "##                 #### #####  ###   ###  ####                      ##"
echo "##                                                                  ##"   
echo "##  brought to you by,                                              ##"   
echo "##             ${AUTHORS}         ##"
echo "##                                                                  ##"   
echo "##  ${PROJECT}    ##"
echo "##                                                                  ##"   
echo "######################################################################"
echo

# check for passed target IP.
if [ $# -eq 1 ]; then
	echo "Checking for host ip passed as command line variable."
	echo
	if valid_ip "$1" || [ "$1" == "$HOST_IP" ]; then
		echo "OpenShift host given is a valid IP..."
		HOST_IP=$1
		echo
		echo "Proceeding with OpenShift host: $HOST_IP..."
		echo
	else
		# bad argument passed.
		echo "Please provide a valid IP that points to an OpenShift installation..."
		echo
		print_docs
		echo
		exit
	fi
elif [ $# -gt 1 ]; then
	print_docs
	echo
	exit
elif [ $# -eq 0 ]; then
	# validate HOST_IP.
  if [ -z ${HOST_IP} ]; then
	  # no host name set yet.
	  echo "No host name set in HOST_IP..."
	  echo
		print_docs
		echo
		exit
	else
		# host ip set, echo and proceed with hostname.
		echo "You've manually set HOST to '${HOST_IP}' so we'll use that for your OpenShift Container Platform target."
		echo
	fi
fi

# make some checks first before proceeding.	
command -v oc version --client >/dev/null 2>&1 || { echo >&2 "OpenShift CLI tooling is required but not installed yet... download here (unzip and put on your path): ${OCP_URL}"; exit 1; }

echo "OpenShift commandline tooling is installed..."
echo 
echo "Logging in to OpenShift as $OPENSHIFT_USER..."
echo
oc login ${HOST_IP}:${HOST_PORT} --password=$OPENSHIFT_PWD --username=$OPENSHIFT_USER

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc login' command!"
	exit
fi

echo
echo "Check for availability of correct version of Red Hat Process Automation Manager Authoring template..."
echo
oc get templates -n openshift rhpam${VERSION}-authoring >/dev/null 2>&1

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc get template rhpam-authoring' command!"
	echo
	echo "Your container platform is mising this tempalte versoin in your catalog: rhpam${VERSION}-authoring"
	echo "Make sure you are using the correct version of Code Ready Containers as listed in project Readme file."
	echo
	exit
fi

echo
echo "Creating a new project..."
echo
oc new-project "$OCP_PRJ"

echo
echo "Setting up a secrets..."
echo
oc process -f support/app-secret-template.yaml | oc create -f -

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc process' app-secret command!"
	echo
	exit
fi

echo
echo "Setting up kieserver secret..."
echo
oc process -f support/app-secret-template.yaml -p SECRET_NAME=kieserver-app-secret | oc create -f -

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc process' kieserver app-secret command!"
	echo
	exit
fi

echo
echo "Setting up business central service account..."
echo
oc create serviceaccount businesscentral-service-account

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc create' bc service account command!"
	echo
	exit
fi

echo
echo "Setting up kieserver service account..."
echo
oc create serviceaccount kieserver-service-account

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc create' kieserver command!"
	echo
	exit
fi

echo
echo "Setting up secrets link to business central service account..."
echo
oc secrets link --for=mount businesscentral-service-account businesscentral-app-secret

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc secrets' link bc service account command!"
	echo
	exit
fi

echo
echo "Setting up secrets link to kieserver service account..."
echo
oc secrets link --for=mount kieserver-service-account kieserver-app-secret

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc secrets' link kieserver service command!"
	echo
	exit
fi


echo
echo "Setting up secrets link for kieserver user and password..."
echo
oc create secret generic rhpam-credentials --from-literal=KIE_ADMIN_USER=${KIE_ADMIN_USER} --from-literal=KIE_ADMIN_PWD=${KIE_ADMIN_PWD}

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc secrets' creating kieserver user and password!"
	echo
	exit
fi

echo
echo "Creating a new application using CRC catalog image..."
echo
oc new-app --template=rhpam$VERSION-authoring \
			-p APPLICATION_NAME="$OCP_APP" \
      -p BUSINESS_CENTRAL_HTTPS_SECRET="businesscentral-app-secret" \
			-p CREDENTIALS_SECRET="rhpam-credentials" \
      -p KIE_SERVER_HTTPS_SECRET="kieserver-app-secret" \
      -p BUSINESS_CENTRAL_MEMORY_LIMIT="$MEM_LIMIT"

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc new-app' command!"
	exit
fi

echo
echo "================================================================================="
echo "=                                                                               ="
echo "=  Login to Red Hat Process Automation Manager to exploring process automation  ="
echo "=  development at:                                                              ="
echo "=                                                                               ="
echo "=  https://${OCP_APP}-rhpamcentr-${OCP_PRJ}.${HOST_APPS}  ="
echo "=                                                                               ="
echo "=    Log in: [ u:erics / p:redhatpam1! ]                                        ="
echo "=                                                                               ="
echo "=    Others:                                                                    ="
echo "=            [ u:kieserver / p:redhatpam1! ]                                    ="
echo "=            [ u:caseuser / p:redhatpam1! ]                                     ="
echo "=            [ u:casemanager / p:redhatpam1! ]                                  ="
echo "=            [ u:casesupplier / p:redhatpam1! ]                                 ="
echo "=                                                                               ="
echo "=  Note: it takes a few minutes to expose the service...                        ="
echo "=                                                                               ="
echo "================================================================================="
echo

